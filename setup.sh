#!/bin/bash
# git clone myconfig.git
# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads/9-2020-q2-update
# https://www.segger.com/downloads/jlink/JLink_Linux_x86_64.tgz

if [[ $EUID -ne 0 ]]; then
   echo "You Must be su"
   exit 1
fi

apps=(
gcc vim tmux htop libxft-dev build-essential 
tig w3m minicom nnn ditaa doxygen pandoc mupdf 
libxinerama1 libxinerama-dev libncurses5 libtool 
pkg-config libusb-1.0-0-dev libhidapi-dev libusb-dev 
libftdi-dev gpiod libgpiod-dev libcapstone-dev neomutt curl
jq cowsay ncurses-dev exuberant-ctags surfraw xinit 
libharfbuzz-dev glade libwebkit2gtk-4.0-dev libgcr-3-dev
x11-xserver-utils feh
)

apt -y update
apt -y upgrade
for var in "${apps[@]}"
do
   echo "****************************************"
   echo Apt installing $var
   echo ----------------------------------------
   apt -y install $var  
   echo "****************************************"
done

usermod -aG dialout $USER 

echo "****************************************"
echo Setting up vim
echo source ~/myconfig/bashrc >> ~/.bashrc
mv -f ~/.vim ~/.vim_old
ln -s -f ~/myconfig/.vim ~/.vim
ln -s -f ~/myconfig/.vim/.vimrc ~/.vimrc
ln -s -f ~/myconfig/.Xdefaults ~/.Xdefaults
ln -s -f ~/myconfig/.tigrc ~/.tigrc
ln -s -f ~/myconfig/.xinitrc ~/.xinitrc
~/myconfig/vimsetup.sh

mkdir -p ~/code
mkdir -p ~/enginuity
cp -fr ~/myconfig/code/suckless ~/code/

# Uncomment to install the downloaded fiels
# cd ~/Downloads/
# tar -xvf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2 
# tar -xvf JLink_Linux_V694a_x86_64.tgz
# sudo mv ./JLink_Linux_V694a_x86_64 /opt/
# sudo ln -s /opt/JLink_Linux_V694a_x86_64 /opt/jlink
# cd ~/myconfig/
#sudo cp -r ~/Downloads/gcc-arm-none-eabi-9-2020-q2-update /opt/
#sudo ln -s  /opt/gcc-arm-none-eabi-9-2020-q2-update /opt/gcc-arm

#cd ~/code/
#git clone https://git.code.sf.net/p/openocd/code openocd-code
#cd ~/code/openocd-code
#./bootstrap
#./configure
#make -j8 
#sudo make install
#cp ~/code/openocd-code/contrib/60-openocd.rules /etc/udev/rules.d/99-openocd.rules 
